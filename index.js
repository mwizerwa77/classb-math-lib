module.exports.add = function add(a, b, c, d) {
  return a + b + c + d;
};
module.exports.sub = function sub(a, b) {
  return a - b;
};
module.exports.multiply = function (a, b) {
  return a * b;
};
module.exports.divide = function (a, b) {
  if (b == 0) {
    throw new Error("divide by 0 is not allowed");
  }
  return a / b;
};
